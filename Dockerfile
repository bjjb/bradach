FROM crystallang/crystal:1.4.0-alpine
ADD . .
RUN shards build --static --production
EXPOSE 9696
CMD ["/bin/bradach", "-d", "-b", "0.0.0.0", "-p", "9696", "-w", "public", "-x", "bjjb.gitlab.io"]
ENTRYPOINT ["/bin/bradach"]
