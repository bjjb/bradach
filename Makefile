.PHONY: test build release

test:
	@crystal spec

build:
	@docker build -t bjjb/bradach .

release: build
	@docker push bjjb/bradach
