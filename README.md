# Bradach

![Faidhg é!][logo]

An API/frontend/client/library.

## Installation

### From [GitLab][]

    go get gitlab.com/bjjb/bradach

### Using [Docker][]

    docker pull bradach

## Features

It's a command-line client, an API service and a JavaScript frontend.

[logo]: https://gitlab.com/bjjb/bradach/raw/master/logo.png "Ahoy!"
[rubygems]: https://gitlab.com
[docker]: https://docker.io/
[GitLab]: https://gitlab.com/bjjb/bradach
[Docker]: https://hub.docker.com/bjjb/bradach
