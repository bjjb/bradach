const $ = (q) => document.querySelector(q)
const $$ = (q) => [...document.querySelectorAll(q)]

$("#search").addEventListener("submit", async (e) => {
	e.preventDefault()
	const headers = { Accept: "application/json" }
	const body = e.target.formData
	const url = new URL(e.target.action)
	const response = await fetch(e.target.action, { headers, body })
	const json = await response.json()
	render(json)
})

fetch("version.json").then(resp => resp.json()).then(({ href, text }) => {
	const a = document.createElement("a")
	a.href = href
	a.title = "Go to the code"
	a.innerHTML = text
	a.target = "new"
	$("footer").append(a)
})
