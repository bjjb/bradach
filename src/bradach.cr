require "uri"
require "json"
require "http/client"
require "option_parser"

module Bradach
  # The version of the program.
  VERSION = "0.1.1"

  # A type of Exception which should be raised by an OptionParser if needed.
  class Error < Exception
  end

  # If true, the results are printed as a JSON array, rather than as a list of
  # magnet links. Has no effect in daemon mode.
  class_property? json : Bool { false }

  # If true, instead of searching, the application will listen on `port`/tcp
  # for new connections and serve JSON responses.
  class_property? daemon : Bool { false }

  # The (max) number of results shown.
  class_property count : Int32 { daemon? ? 10 : 1 }

  # A version string for the programme.
  class_property version : String { "#{PROGRAM_NAME} v#{VERSION}" }

  # The address on which to listen (when daemon is true)
  class_property addr : String { "127.0.0.1" }

  # The port on which to listen (when daemon is true)
  class_property port : Int32 { 9696 }

  # An origin to use for CORS
  class_property cors : String { "" }

  # A directory from which to serve static files (in daemon mode).
  class_property webroot : String { "" }

  # The URL used to search. Can be set with $BRADACH_API_URL.
  class_property url : String do
    ENV.fetch("BRADACH_API_URL") { "https://apibay.org/q.php" }
  end

  # The list of trackers used to create magnet links.
  class_property trackers : Array(String) do
    %w(
      udp://9.rarbg.me:2780/announce
      udp://9.rarbg.to:2710/announce
      udp://9.rarbg.to:2730/announce
      udp://open.stealth.si:80/announce
      udp://tracker.coppersurfer.tk:6969/announce
      udp://tracker.openbittorrent.com:6969/announce
      udp://tracker.opentrackr.org:1337/announce
      udp://tracker.tiny-vps.com:6969/announce
      udp://tracker.torrent.eu.org:451/announce
    )
  end

  # Parses the given command-line args using an OptionParser.
  def parse(args : Array(String) = ARGV)
    parser.parse(args)
  end

  # Adds the banner and description separator to op.
  private def describe(op : OptionParser)
    op.banner = "Usage: #{PROGRAM_NAME} [flags] QUERY"
    op.separator "
Searches its indices for results, and prints the top one (or the top N if
`-n/--count` is used). By default it prints a result as a magnet link, however
you can use the `-j/--json` flag to get the results in JSON instead.

Flags:
"
  end

  # Adds the -h/--help flag to op, which simply prints op.
  private def help(op : OptionParser)
    op.on "-h", "--help", "print help and exit" do
      op.before_each { |x| raise Error.new("Unexpected: #{x}") }
      op.unknown_args { |_x, _y| puts op }
    end
  end

  # Adds the -V/--version flag to op, which simply prints Bradach.version and
  # exits.
  private def version(op : OptionParser)
    op.on "-V", "--version", "print version info (and exit)" do
      op.before_each { |x| raise Error.new("Unexpected: #{x}") }
      op.unknown_args { |_x, _y| puts version }
    end
  end

  # Adds the -n/--count flag to op, which sets Bradach.count to N.
  private def count(op : OptionParser)
    op.on "-n", "--count N", "show up to N results" do |n|
      self.count = Int32.new(n)
    end
  end

  # Adds the -j/--json flag to op, which sets Bradach.json.
  private def json(op : OptionParser)
    op.on "-j", "--json", "show results as a JSON array" do
      self.json = true
    end
  end

  # Adds the -d/--daemon option to op, which toggles `daemon`. This also adds
  # the -x/cors, -w/--webroot, -p/--port and -b/--bind options.
  private def daemon(op : OptionParser)
    op.on "-d", "--daemon", "start a server" do
      op.on "-b", "--bind ADDR", "TCP listener address" do |addr|
        self.addr = addr
      end
      op.on "-p", "--port NUM", "TCP listener port" do |n|
        self.port = Int32.new(n)
      end
      op.on "-x", "--cors ORIGIN", "allow CORS from ORIGIN" do |origin|
        self.cors = origin
      end
      op.on "-w", "--webroot DIR", "serve static files from DIR" do |dir|
        raise Error.new("Invalid webroot") unless File.directory?(dir)
        self.webroot = dir
      end
      op.unknown_args do |before, after|
        server = self.server
        addr = server.bind_tcp(self.addr, port)
        puts "#{PROGRAM_NAME} v#{VERSION} listening on http://#{addr}..."
        server.listen
      end
    end
  end

  # Adds the default action to op. If there are no arguments, it will simply
  # raise op as an Error. Otherwise, it will
  private def run(op : OptionParser)
    op.unknown_args do |x, y|
      raise Error.new(op.to_s) if x.empty?
      raise Error.new("Unexpected args after -- #{y}") unless y.empty?
      results = search(x.join(" ")).first(count)
      puts results.to_json if json?
      results.each { |r| puts r.uri } unless json?
    end
  end

  # Returns the middleware used by a server
  private def middleware
    middleware = [] of HTTP::Handler
    middleware.push HTTP::ErrorHandler.new
    middleware.push HTTP::LogHandler.new
    middleware.push HTTP::CompressHandler.new
    if webroot != ""
      middleware.push(HTTP::StaticFileHandler.new(webroot, true, false))
    end
    middleware.push(CORS.new(cors)) if cors != ""
    middleware
  end

  # Returns true if the given mime type is acceptible for the header.
  private def accepts?(mime_type : String, header : String) : Bool
    true
  end

  # Handles a HTTP::Server::Context.
  private def handle(context : HTTP::Server::Context)
    unless context.request.path == "/search"
      context.response.status = HTTP::Status::NOT_FOUND
      return
    end
    unless accepts?("application/json", context.request.headers["Accept"])
      context.response.status = HTTP::Status::NOT_ACCEPTABLE
      return
    end
    n = Int32.new(context.request.query_params.fetch("n") { 1 })
    queries = context.request.query_params.fetch_all("q")
    if queries.empty?
      context.response.status = HTTP::Status::BAD_REQUEST
      return
    end
    results = queries.reduce([] of Result) do |r, q|
      r.concat(search(q).sort.first(n))
    rescue e : Error
      r
    end
    context.response.content_type = "application/json"
    context.response.print results.to_json
  end

  # Returns a server that can be bound to a TCP port and run when in `daemon`
  # mode.
  private def server
    HTTP::Server.new(middleware) { |context| handle(context) }
  end

  # Builds a new OptionParser.
  private def parser
    OptionParser.new do |op|
      describe(op)
      help(op)
      version(op)
      json(op)
      count(op)
      daemon(op)
      run(op)
    end
  end

  # Performs an actual search.
  private def search(q : String) : Array(Result)
    uri = URI.parse(url)
    uri.query = URI::Params.encode({q: q})
    response = HTTP::Client.get(uri)
    unless response.success?
      raise Error.new("Bad response (#{response.status_code})")
    end
    results = Array(Result).from_json(response.body)
    if results.size == 1 && results.first.id == "0"
      raise Error.new("No results.")
    end
    results
  end

  # An encapsulation of the results received from the API, with an additional
  # `uri` method which builds a magnet link (using the `info_hash` and the list
  # of trackers).
  class Result
    include JSON::Serializable
    getter id : String?
    getter name : String?
    getter info_hash : String?
    getter leechers : String?
    getter seeders : String
    getter num_files : String?
    getter size : String?
    getter username : String?
    getter added : String?
    getter status : String?
    getter category : String?
    getter imdb : String?

    getter uri : String { "magnet:?#{params.join("&")}" }
    getter params : Array(String) do
      [
        "xt=urn:btih:#{info_hash}",
        "dn=#{URI.encode_www_form(name || "Unnamed")}",
        *Bradach.trackers.map { |tr| "tr=#{tr}" },
      ]
    end

    # Comparable
    def <=>(other)
      Int32.new(other.seeders) <=> Int32.new(seeders)
    end
  end

  # A little CORS HTTP::Handler for allowing JSON
  class CORS
    include HTTP::Handler

    def initialize(@origin : String)
    end

    def call(ctx : HTTP::Server::Context)
      ctx.response.headers["Access-Control-Allow-Methods"] = "GET"
      ctx.response.headers["Access-Control-Allow-Headers"] = "Content-Type"
      ctx.response.headers["Access-Control-Allow-Origin"] = @origin
      return call_next(ctx) unless ctx.request.method.downcase == "options"
      ctx.response.respond_with_status HTTP::Status::OK
    end
  end

  extend self
end
