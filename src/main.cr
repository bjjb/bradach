require "./bradach"

begin
  Bradach.parse(ARGV)
rescue e : Bradach::Error
  puts e.message
  exit 1
end
